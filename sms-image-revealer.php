<?php
  /*
Plugin Name: Image Revealer 
Plugin URI: http://www.roadsidemultimedia.com
Description: Image Revealer for DMS
Author: Curtis Grant
PageLines: true
Version: 1.3
Section: true
Class Name: ImageRevealer
Filter: components, custom
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-image-revealer
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;

global $ir_num;
$ir_num = 0;
class ImageRevealer extends PageLinesSection {

  function section_styles(){
    
    wp_enqueue_script( 'curved-js', $this->base_url.'/js/curved-text.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_script( 'jquery-lettering-js', $this->base_url.'/js/jquery.lettering.min.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_script( 'jquery-mobile-custom-js', $this->base_url.'/js/jquery.mobile.custom.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_style( 'ir-css', $this->base_url.'/css/ir.css');

  }
  function section_persistent(){
    
  }

  function section_opts(){

    $opts = array(
      array(
            'type'  => 'image_upload',
            'key' => 'ir_front',
            'label' => __( 'Front Image', 'pagelines' ),
            'has_alt' => false,
            'col' => 1,
            ),
      array(
            'type'  => 'image_upload',
            'key' => 'ir_back',
            'label' => __( 'Back Image', 'pagelines' ),
            'has_alt' => false,
            'col' => 2,
            ),
      array(
            'type'      => 'select',
            'key'     => 'curvedtext',
            'label'     => 'Use curved Text',
            'col' => 1,
            'opts'      => array(
              'yes'   => array('name' => 'Yes'),
              'no'  => array('name' => 'No'),
            )
          ),
      array(
        'key' => 'ir_text',
        'type'  => 'text',
        'label' => __( 'Surrounding Text(Desktop)', 'pagelines' ),
        'col' => 1,
            ),
      array(
        'key' => 'ir_mtext',
        'type'  => 'text',
        'label' => __( 'Surrounding Text (Mobile)', 'pagelines' ),
        'col' => 1,
            ),
      array(
            'type'      => 'select',
            'key'     => 'ir_text_lr',
            'label'     => 'Left/Right',
            'col' => 2,
            'opts'      => array(
              'ircurvedtextleft'   => array('name' => 'Left'),
              'ircurvedtext'  => array('name' => 'Right'),
            )
          ),
    );

    return $opts;

  }

  /**
  * Section template.
  */
   function section_template( $location = false ) {

    // Global Variables (pl_setting)
    $irfront = ( $this->opt('ir_front') ) ? $this->opt('ir_front') : "";
    $irback = ( $this->opt('ir_back') ) ? $this->opt('ir_back') : "";
    $irtext = ( $this->opt('ir_text') ) ? $this->opt('ir_text') : "Hover over Actual Patient Photo to see Before Image.";
    $irtextm = ( $this->opt('ir_mtext') ) ? $this->opt('ir_mtext') : "Tap Actual Patient Photo to toggle Before/After Image";
    $curved_text = ( $this->opt('curvedtext') ) ? $this->opt('curvedtext') : "yes";
    $irtextlr = ( $this->opt('ir_text_lr') ) ? $this->opt('ir_text_lr') : "ircurvedtext";
    if ($irtextlr=="ircurvedtext") {
      $iralign = "irtextleft";
    }
    else {
      $iralign = "irtextright";
    }
    global $ir_num;
   if(!$ir_num) {
     $ir_num = 1;
   }
    // Check for any mobile device, and tablets.
?>
    <script type="text/javascript">
      jQuery(function($) {
        $(document).ready(function() {
          
        if (Modernizr.touch) {
           $(".irgrid.num<?php echo $ir_num; ?>").tap(function(){
            $(".irgrid.num<?php echo $ir_num; ?> .irback").toggleClass("visible");
          });
           <?php if ($curved_text == 'yes') { ?>
              $(".id<?php echo $ir_num; ?>.irtextm").show();
              $(".id<?php echo $ir_num; ?>.irtext").hide();

           <?php } else { ?>
              $(".id<?php echo $ir_num; ?>.irtextm").hide();
              $(".id<?php echo $ir_num; ?>.irtext").hide();
           <?php } ?>
        }
        else {
          $('#<?php echo $irtextlr; ?>.id<?php echo $ir_num; ?>').circleType({position: 'relative',dir: -1, radius: 175});
          $(".irgrid.num<?php echo $ir_num; ?>").hover( function(){
            $(".irgrid.num<?php echo $ir_num; ?> .irback").toggleClass("visible");
          });
           <?php if ($curved_text == 'yes') { ?>
              $(".id<?php echo $ir_num; ?>.irtext").show();
              $(".id<?php echo $ir_num; ?>.irtextm").hide();
           <?php } else { ?>
              $(".id<?php echo $ir_num; ?>.irtextm").hide();
              $(".id<?php echo $ir_num; ?>.irtext").hide();
           <?php } ?>
        }
          
        });
      });
    </script>
  <div id="ircontainer" class="<?php if ($curved_text == 'yes') { echo "withcurve"; } else { echo "withoutcurve"; } ?>"> 
    <ul class="irgrid num<?php echo $ir_num; ?> <?php echo $iralign; ?>" >
      <li>
        <div class="irfront" style="background: url('<? echo $irfront; ?>') center;">
          <div class="irback info-3"  style="background: url('<? echo $irback; ?>') center;"></div>
        </div>
      </li>
    </ul>
    <div id="<?php echo $irtextlr; ?>" class="id<?php echo $ir_num; ?> irtext"><?php echo $irtext; ?></div>
    <div id="<?php echo $irtextlr; ?>" class="id<?php echo $ir_num; ?> irtextm"><?php echo $irtextm; ?></div>
 </div>
<?php 
$ir_num++;
}

}


